import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Framing
import akka.stream.scaladsl.FileIO
import akka.util.ByteString

import scala.io.Source

//import java.nio.file.Path
//
//import akka.stream.scaladsl.{FileIO, Framing}
//import akka.util.ByteString
//import java.nio.file.Paths
//
//import akka.actor.ActorSystem
//import akka.stream.ActorMaterializer
//
//import scala.io.Source
//

//val fileIterator = Source.fromFile("/Users/vgrygoruk/github/vgrigoruk/logtracer/data/small-log.txt").getLines
//fileIterator.map(LogEntryParser.parse).foreach {
//  case Some(entry) => println(entry.start.getNano)
//  case None =>
//}


implicit val system = ActorSystem("logtracer")
implicit val materializer = ActorMaterializer()

FileIO.fromPath(Paths.get("data/small-log.txt"))
  .via(Framing.delimiter(ByteString("\n"), 256, true)
  .map(_.utf8String))
  .runForeach(println)




////
////FileIO.fromPath("/Users/vgrygoruk/Downloads/logtracer/data/small-log.txt")
//
//
////via(delimiter(ByteString("\n"), Int.MaxValue)). // split into lines
////  map(_.decodeString("UTF-8")). // convert ByteStrings to Strings
////  filter(!_.startsWith("#")). // remove comments
////  fold(0)((i, _) => i + 1). // count remaining lines
//
//implicit val system = ActorSystem("logtracer")
//implicit val materializer = ActorMaterializer()
//
////FileIO.
////  fromPath(Paths.get("/Users/vgrygoruk/Downloads/logtracer/data/small-log.txt")).
////  runForeach(println) // run the stream, print result
//
//FileIO.fromPath(Paths.get("small-log.txt"))
//  .via(Framing.delimiter(ByteString("\n"), 256, true)
//  .map(_.utf8String))
//  .runForeach(println)
//
//
