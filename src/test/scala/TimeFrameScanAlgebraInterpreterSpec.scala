import java.time.Instant
import java.time.temporal.ChronoUnit.SECONDS
import java.util.concurrent.TimeUnit

import org.home.logtracer.{LogEntry, TimeFrame, TimeFrameScanAlgebraInterpreter, Trace}
import org.scalatest.FunSpec

import scala.concurrent.duration.FiniteDuration

class TimeFrameScanAlgebraInterpreterSpec extends FunSpec {
  import TimeFrameScanAlgebraInterpreter._

  describe("org.home.logtracer.TimeFrameScanAlgebra") {
    describe("#takeTimeWindow") {

      it("should produce empty Ready and TimeWindowBuffer if timeWindow timeframe is empty") {
        val timeWindow = new FiniteDuration(10, TimeUnit.SECONDS)
        val startTime = Instant.now()
        val results = TimeFrameScanAlgebraInterpreter.takeTimeWindow(timeWindow, TimeFrame(Map()).asInstanceOf[TimeFrame with Batch])
        assert(results._1.traces.isEmpty) // Ready
        assert(results._2.traces.isEmpty) // TimeWindowBuffer
      }

      it("should produce Ready traces from all logEntries in case of 0 timeWindow") {
        val timeWindow = new FiniteDuration(0, TimeUnit.SECONDS)
        val startTime = Instant.now()
        val logEnties = List(
          LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "span1", "span2"),
          LogEntry(startTime, startTime.plus(1, SECONDS), "cid1", "s2", "span2", "span3"),
          LogEntry(startTime.plus(1, SECONDS), startTime.plus(2, SECONDS), "cid1", "s3", "span2", "span4")
        )
        val results = TimeFrameScanAlgebraInterpreter.takeTimeWindow(timeWindow, TimeFrame.from(logEnties).asInstanceOf[TimeFrame with Batch])
        assert(results._1.traces.size == 1) // Ready
        assert(results._1.traces.values.flatMap(_.logEntries).size == 3)
        assert(results._2.traces.isEmpty) // TimeWindowBuffer
      }

      it("should produce empty Ready and All entries should go to TimeWindowBuffer if timeWindow is larger than trace timespan") {
        val timeWindow = new FiniteDuration(10, TimeUnit.SECONDS)
        val startTime = Instant.now()
        val logEnties = List(
          LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "span1", "span2"),
          LogEntry(startTime, startTime.plus(1, SECONDS), "cid1", "s2", "span2", "span3"),
          LogEntry(startTime.plus(1, SECONDS), startTime.plus(2, SECONDS), "cid1", "s3", "span2", "span4")
        )
        val results = TimeFrameScanAlgebraInterpreter.takeTimeWindow(timeWindow, TimeFrame.from(logEnties).asInstanceOf[TimeFrame with Batch])
        assert(results._1.traces.isEmpty) // Ready
        assert(results._2.traces.size == 1) //TimeWindowBuffer
        assert(results._2.traces.values.flatMap(_.logEntries).size == 3)
      }

      it("should put logentries from 1 trace into TimeWindowBuffer if timeWindow overlaps with trace time") {
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val startTime = Instant.now()
        val logEnties = List(
          LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "span1", "span2"),
          LogEntry(startTime, startTime.plus(1, SECONDS), "cid1", "s2", "span2", "span3"),
          LogEntry(startTime.plus(1, SECONDS), startTime.plus(2, SECONDS), "cid1", "s3", "span2", "span4")
        )
        val results = TimeFrameScanAlgebraInterpreter.takeTimeWindow(timeWindow, TimeFrame.from(logEnties).asInstanceOf[TimeFrame with Batch])
        assert(results._1.traces.isEmpty) // Ready
        assert(results._2.traces.size == 1)
        assert(results._2.traces.values.flatMap(_.logEntries).size == 3)
      }

      it("should put all traces that do not overlap with timeWindow into a Result") {
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val startTime = Instant.now()
        val logEnties = List(
          LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "null", "span2"),
          LogEntry(startTime, startTime.plus(1, SECONDS), "cid1", "s2", "span2", "span3"),
          LogEntry(startTime.plus(1, SECONDS), startTime.plus(2, SECONDS), "cid1", "s3", "span2", "span4"),
          LogEntry(startTime, startTime.plus(4, SECONDS), "cid2", "s1", "null", "b2"),
          LogEntry(startTime, startTime.plus(2, SECONDS), "cid2", "s2", "b2", "b3"),
          LogEntry(startTime.plus(2, SECONDS), startTime.plus(3, SECONDS), "cid2", "s3", "b2", "b5")
        )
        val results = TimeFrameScanAlgebraInterpreter.takeTimeWindow(timeWindow, TimeFrame.from(logEnties).asInstanceOf[TimeFrame with Batch])
        assert(results._1.traces.size == 1) // Ready
        assert(results._2.traces.size == 1)
      }
    }

    describe("#join") {
      /**
        *  timeWindowBuffer  | ---
        *  nextBatch         |     ---
        */
      it("joins 2 different traces and produces empty TimedOut org.home.logtracer.TimeFrame if next batch starts later than previous (TimeWindowBuffer)") {
        val startTime = Instant.now()
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val buffer = TimeFrame(
          Map(
            "cid" -> Trace(
              List(
                LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with TimeWindowBuffer]
        val batch = TimeFrame(
          Map(
            "cid2" -> Trace(
              List(
                LogEntry(startTime.plus(2, SECONDS), startTime.plus(4, SECONDS), "cid2", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with Batch]
        val results = TimeFrameScanAlgebraInterpreter.join(timeWindow, buffer, batch)
        assert(results._1.traces.isEmpty)
        assert(results._2.traces.size == 2)
      }

      /**
        *  timeWindowBuffer  |      ---
        *  nextBatch         | ---
        */
      it("puts Next Batch into TimeOut if next batch is to the left of previous (TimeWindowBuffer)") {
        val startTime = Instant.now()
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val buffer = TimeFrame(
          Map(
            "cid" -> Trace(
              List(
                LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with TimeWindowBuffer]
        val batch = TimeFrame(
          Map(
            "cid2" -> Trace(
              List(
                LogEntry(startTime.minus(2, SECONDS), startTime.minus(1, SECONDS), "cid2", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with Batch]
        val results = TimeFrameScanAlgebraInterpreter.join(timeWindow, buffer, batch)
        assert(results._1.traces.size == 1)
        assert(results._2.traces.size == 1)
      }

      /**
        *  timeWindowBuffer  | ---
        *  nextBatch         | (empty)
        */
      it("merges timeWindowBuffer with the next empty batch") {
        val startTime = Instant.now()
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val buffer = TimeFrame(
          Map(
            "cid" -> Trace(
              List(
                LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with TimeWindowBuffer]
        val batch = TimeFrame().asInstanceOf[TimeFrame with Batch]
        val results = TimeFrameScanAlgebraInterpreter.join(timeWindow, buffer, batch)
        assert(results._1.traces.isEmpty)
        assert(results._2.traces.size == 1)
      }

      /**
        *  timeWindowBuffer  | (empty)
        *  nextBatch         |  ---
        */
      it("merges empty TimeWindowBuffer with the next batch") {
        val startTime = Instant.now()
        val timeWindow = new FiniteDuration(1, TimeUnit.SECONDS)
        val buffer = TimeFrame().asInstanceOf[TimeFrame with TimeWindowBuffer]
        val batch = TimeFrame(
          Map(
            "cid" -> Trace(
              List(
                LogEntry(startTime, startTime.plus(2, SECONDS), "cid1", "s1", "null", "span2")
              )
            )
          )
        ).asInstanceOf[TimeFrame with Batch]
        val results = TimeFrameScanAlgebraInterpreter.join(timeWindow, buffer, batch)
        assert(results._1.traces.isEmpty)
        assert(results._2.traces.size == 1)
      }
    }
  }
}
