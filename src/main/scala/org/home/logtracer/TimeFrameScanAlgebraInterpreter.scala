package org.home.logtracer

import java.time.Instant

import scala.concurrent.duration.FiniteDuration

object TimeFrameScanAlgebraInterpreter extends TimeFrameScanAlgebra[TimeFrame] {

  override def takeTimeWindow(timeWindow: FiniteDuration, batch: DataFrame[Batch]): (DataFrame[Ready], DataFrame[TimeWindowBuffer]) = {
    val partitionPoint = batch.end.minusMillis(timeWindow.toMillis)
    val partitions = batch.traces.partition { case (_, trace) => trace.end.isAfter(partitionPoint) }
    (TimeFrame(partitions._2).asInstanceOf[TimeFrame with Ready], TimeFrame(partitions._1).asInstanceOf[TimeFrame with TimeWindowBuffer])
  }

  override def join(timeWindow: FiniteDuration, buffer: DataFrame[TimeWindowBuffer], next: DataFrame[Batch]): (DataFrame[Outliers], DataFrame[Batch]) = {
    val partitionPoint = Instant.ofEpochMilli(math.min(buffer.end.minusMillis(timeWindow.toMillis).toEpochMilli, buffer.start.toEpochMilli))
    val partitions = next.traces.values.flatMap(_.logEntries).partition(logEntry => logEntry.end.isBefore(partitionPoint))

    val timedOut = TimeFrame.from(partitions._1.toList).asInstanceOf[TimeFrame with Outliers]
    val chunk = TimeFrame.merge(buffer, TimeFrame.from(partitions._2.toList)).asInstanceOf[TimeFrame with Batch]
    (timedOut, chunk)
  }

  override def empty[A]: DataFrame[A] = TimeFrame().asInstanceOf[TimeFrame with A]
}
