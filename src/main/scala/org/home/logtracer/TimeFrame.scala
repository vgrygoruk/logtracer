package org.home.logtracer

import java.time.Instant

case class TimeFrame private (traces: Map[String, Trace] = Map()) {
  lazy val start: Instant = if (traces.nonEmpty) traces.values.map(_.start).min else Instant.EPOCH
  lazy val end: Instant = if (traces.nonEmpty) traces.values.map(_.end).max else Instant.EPOCH
}


object TimeFrame {
  def from(seq:List[LogEntry]) : TimeFrame = TimeFrame(seq.groupBy(_.correlationId).mapValues(Trace.apply))

  def merge(left: TimeFrame, right: TimeFrame): TimeFrame = {
    TimeFrame(mergeMaps(Seq(left.traces, right.traces)) { (_, v1, v2) => Trace(v1.logEntries ::: v2.logEntries) })
  }

  private def mergeMaps[K, V](maps: Seq[Map[K, V]])(f: (K, V, V) => V): Map[K, V] = {
    maps.foldLeft(Map.empty[K, V]) { case (merged, m) =>
      m.foldLeft(merged) { case (acc, (k, v)) =>
        acc.get(k) match {
          case Some(existing) => acc.updated(k, f(k, existing, v))
          case None => acc.updated(k, v)
        }
      }
    }
  }
}



