package org.home.logtracer

import java.time.Instant

import cats.Show

import scala.util.{Failure, Success, Try}

case class LogEntry(start: Instant,
                    end: Instant,
                    correlationId: String,
                    serviceName: String,
                    callerSpan: String,
                    span: String)

object LogEntry {
  type ErrorOr[A] =  Either[String, A]

  implicit val showLogEntry: Show[LogEntry] {
    def show(t: LogEntry): String
  } = new Show[LogEntry] {
    override def show(t: LogEntry): String = t.toString
  }

  def fromString(input: String) : ErrorOr[LogEntry] = {
    val items = input.split(' ')
    Try(LogEntry(
      Instant.parse(items(0)),
      Instant.parse(items(1)),
      items(2),
      items(3),
      items(4).split("->")(0),
      items(4).split("->")(1)
    )) match {
      case Success(logEntry) => Right(logEntry)
      case Failure(_) => Left(input)
    }
  }
}
