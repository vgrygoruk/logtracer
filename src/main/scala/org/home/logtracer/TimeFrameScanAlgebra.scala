package org.home.logtracer

import scala.concurrent.duration.FiniteDuration

trait TimeFrameScanAlgebra[TimedDataFrame]{
  type DataFrame[A] = TimedDataFrame with A
  type ResultFrame = (DataFrame[Outliers], DataFrame[Ready])
  trait TimeWindowBuffer
  trait Batch
  trait Ready
  trait Outliers

  def empty[A] : DataFrame[A]

  def takeTimeWindow(timeWindow:FiniteDuration, data:DataFrame[Batch]) : (DataFrame[Ready], DataFrame[TimeWindowBuffer])

  def join(timeWindow:FiniteDuration, previous:DataFrame[TimeWindowBuffer], next:DataFrame[Batch]) : (DataFrame[Outliers], DataFrame[Batch])

  def scan(timeWindow:FiniteDuration, buffer:DataFrame[TimeWindowBuffer], next:DataFrame[Batch]) :
  (DataFrame[Outliers], DataFrame[Ready],  DataFrame[TimeWindowBuffer]) = {
    val (outlier, frameToAnalyse) = join(timeWindow, buffer, next)
    val (processed, nextBuffer) = takeTimeWindow(timeWindow, frameToAnalyse)
    (outlier, processed, nextBuffer)
  }

  import fs2._

  import scala.language.higherKinds

  def scanStream[F[_]](timeWindow:FiniteDuration) : Pipe[F, DataFrame[Batch], ResultFrame]  = {
    def goRecursive(buffer: DataFrame[TimeWindowBuffer], s:Stream[F, DataFrame[Batch]]) : Pull[F, ResultFrame, Unit] = {
      s.pull.uncons1.flatMap{
        case Some((dataFrame, tailStream)) =>
          val (outlier, processed, nextBuffer) = scan(timeWindow, buffer, dataFrame)
          Pull.segment[F, ResultFrame, Unit](Segment(Tuple2(outlier,processed))).flatMap( _ => goRecursive(nextBuffer, tailStream) )
        case None =>
          Pull.segment[F, ResultFrame, Unit](Segment(Tuple2(empty[Outliers], buffer.asInstanceOf[DataFrame[Ready]])))
            .flatMap(_ => Pull.done)
      }
    }
    in => goRecursive(empty[TimeWindowBuffer], in).stream
  }
}
