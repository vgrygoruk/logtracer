package org.home.logtracer

import java.time.Instant

case class Trace(logEntries: List[LogEntry]) {
  val start: Instant = logEntries.minBy(_.start).start
  val end: Instant = logEntries.maxBy(_.end).end
}

object Trace {
  import io.circe.{Encoder, _}
  import io.circe.syntax._

  implicit val instantEncoder : Encoder[Instant] = new Encoder[Instant] {
    override def apply(a: Instant) = Json.fromString(a.toString)
  }

  implicit val traceEncoder : Encoder[Trace] = new Encoder[Trace]{

    override def apply(a: Trace): Json = toJson(a)

    private [this] def toJsonRecursively(caller:LogEntry, list:List[LogEntry]) : (Json, List[LogEntry]) = {

      val (callEntries, tail) = list.partition(_.callerSpan == caller.span)

      val (calls, remaining) = callEntries.map(log => toJsonRecursively(log, tail))
        .foldLeft(Seq.empty[Json] -> List.empty[LogEntry]){
          (accum, next) => Tuple2(accum._1 :+ next._1, accum._2 ++ next._2)
        }

      val json = Json.fromFields(Seq(
        "service" -> caller.serviceName.asJson,
        "start" -> caller.start.asJson,
        "end" -> caller.end.asJson,
        "span" -> caller.span.asJson,
        "calls" -> calls.asJson ))
      (json, remaining)
    }


    private [this] def toJson(trace: Trace) : Json = {
      val root = trace.logEntries.find(_.callerSpan == "null").getOrElse(trace.logEntries.minBy(_.start))
      Json.fromFields(Seq(
        "id" -> root.correlationId.asJson,
        "root" -> toJsonRecursively(root, trace.logEntries.filter(_.callerSpan != "null"))._1)
      )
    }

  }
}
