package org.home.logtracer

/**
  * Created by vgrygoruk on 11/03/2018.
  */
object Launcher extends App {

    import java.nio.file.Paths

    import LogEntry._
    import TimeFrameScanAlgebraInterpreter._
    import cats.effect._
    import cats.instances.either._
    import cats.instances.string._
    import fs2.{Pipe, Sink, Stream, text, io => fs2io}
    import io.circe.syntax._

    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.concurrent.duration._

    val logsPerDatFrame = 100
    val timeFrameDuration = 1.second
    val inputFile = "data/medium-log.txt"
    val outputFile = "data/out.txt"

    fs2io.file
      .readAll[IO](Paths.get(inputFile), 100*logsPerDatFrame)
      .chunks.through(text.utf8DecodeC)
      .through(text.lines)
      .filter(_.nonEmpty)
      .map(LogEntry.fromString)
      .observe(collect[IO, ErrorOr[LogEntry], String] { case Left(msg) => s"[PARSING ERROR] $msg" } andThen Sink.showLinesStdOut[IO, String])
      .collect{case Right(log) => log}
      .segmentN(logsPerDatFrame)
      .map(segment => TimeFrame.from(segment.force.toList).asInstanceOf[TimeFrame with Batch])
      .through[ResultFrame](TimeFrameScanAlgebraInterpreter.scanStream[IO](timeFrameDuration))
      .observe(collect[IO, ResultFrame, String]{case (out, ready) if out.traces.nonEmpty => out.toString} andThen Sink.showLinesStdOut[IO, String])
      .collect{case (out, ready) => ready}
      .flatMap(dataFrame =>  Stream.fromIterator[IO, (String, Trace)](dataFrame.traces.toList.toIterator) )
      .map(_._2.asJson.noSpaces)
      .intersperse("\n")
      .through(text.utf8Encode)
      .to(fs2.io.file.writeAll[IO](Paths.get(outputFile)))
      .compile.drain.unsafeRunSync()

    def collect[F[_], A, B](collect:PartialFunction[A,B]) : Pipe[F, A, B] = _.collect(collect)
}
