name := "logtracer"

version := "1.0"

//scalaVersion := "2.12.4"
//
//libraryDependencies ++= Seq(
//  "co.fs2" %% "fs2-core" % "0.10.0-RC1",
//  "co.fs2" %% "fs2-io" % "0.10.0-RC1",
//  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
//)


scalaVersion := "2.11.11"
val http4sVersion = "0.18.1"

val circeVersion = "0.9.1"
val fs2Version = "0.10.1"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-optics" % circeVersion,
  "org.scalatest" %% "scalatest" % "2.2.6" % Test,
  "org.scalacheck" %% "scalacheck" % "1.13.4" % Test,
  "co.fs2" %% "fs2-core" % fs2Version,
  "co.fs2" %% "fs2-io" % fs2Version
)
