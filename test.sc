//import fs2.{Pipe, Pull, Segment}

//def lastCharIsMergedWithNext[F[_]]() : Pipe[F, String, String]  = {
//
//  def go(prefix:String, s:Stream[F, String]) : Pull[F, String, Unit] = {
//    // Pull represents command for stream driver,
//    // commands could be chained into "list-like" structure which driver will go through and execute
//    // let's imagine we are going to have dialogue with stream driver
//
//    s.pull //me: hey driver! give me the list of available commands you could execute on the stream
//      .uncons1 //me: okay I choose uncons1, which means -  take (pull) first element from the stream
//      .flatMap{ //driver: well first element is wrapped in the Pull, but Pull is a monad! just flatMap ont it, if you want to get access to the value
//      case Some((el, tailStream)) => // driver: take the element (which I've pulled from the stream) and ref to the tail of the stream
//        // driver: hey user!.. and... do what you want with them but don't forget to return the next command;
//        Pull.segment(Segment(prefix+el.toUpperCase)) // me: okay, output this segment
//          .flatMap( _ => go(el.last.toString, tailStream) ) // me: and then execute the command returned from go(...) function
//      // me: ha-ha! I got you... you caught in to recursion! never trust me again)));
//      // driver: I'm not stupid, any way you give me back something like: "Pull -> Pull -> Pull" so I know how to avoid StackOverflow
//      case None => Pull.done // me: don't panic driver, if no element to pull from stream then you are done;
//    }
//  }
//
//  in => go("", in).stream // me: hey driver! you have your Pull commands ("execution plan"), so give me back the stream and good bye...
//}
